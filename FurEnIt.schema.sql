PRAGMA user_version = 65536; -- (1 << 16) + (0 << 8) + 0
PRAGMA foreign_keys = on;
PRAGMA encoding = 'UTF-8';

CREATE VIEW schema_version AS
SELECT
  user_version >> 16 & 0xFF AS major,
  user_version >> 8 & 0xFF AS minor,
  user_version & 0xFF AS bugfix
FROM pragma_user_version();

CREATE TABLE languages (
  code TEXT PRIMARY KEY NOT NULL,
  name_fur TEXT NOT NULL,
  name_en TEXT NOT NULL,
  name_it TEXT NOT NULL
);

CREATE TABLE parts_of_speech (
  id TEXT PRIMARY KEY NOT NULL,
  name_fur TEXT NOT NULL,
  short_fur TEXT NOT NULL,
  name_en TEXT NOT NULL,
  short_en TEXT NOT NULL,
  name_it TEXT NOT NULL,
  short_it TEXT NOT NULL
);

CREATE TABLE genders (
  id TEXT PRIMARY KEY NOT NULL,
  name_fur TEXT NOT NULL,
  short_fur TEXT NOT NULL,
  name_en TEXT NOT NULL,
  short_en TEXT NOT NULL,
  name_it TEXT NOT NULL,
  short_it TEXT NOT NULL
);

CREATE TABLE numbers (
  id TEXT PRIMARY KEY NOT NULL,
  name_fur TEXT NOT NULL,
  short_fur TEXT NOT NULL,
  name_en TEXT NOT NULL,
  short_en TEXT NOT NULL,
  name_it TEXT NOT NULL,
  short_it TEXT NOT NULL
);

CREATE TABLE meanings (
  id INTEGER PRIMARY KEY NOT NULL,
  description_fur TEXT NOT NULL,
  description_en TEXT NOT NULL,
  description_it TEXT NOT NULL,
  description_source TEXT
);

CREATE TABLE words (
  id INTEGER KEY,
  word TEXT COLLATE NOCASE NOT NULL,
  pronunciation TEXT NOT NULL,
  language TEXT NOT NULL,
  part_of_speech TEXT NOT NULL,
  FOREIGN KEY (language) REFERENCES languages (code),
  FOREIGN KEY (part_of_speech) REFERENCES parts_of_speech (id)
);
CREATE INDEX words_idx ON words (word);
CREATE INDEX word_initials_idx ON words (substr(word, 1, 1) COLLATE NOCASE);
CREATE INDEX word_languages_idx ON words (language);
CREATE INDEX word_parts_of_speech_idx ON words (part_of_speech);

CREATE TABLE words_have_meaning (
  word_id INTEGER NOT NULL,
  meaning_id INTEGER NOT NULL,
  examples TEXT NOT NULL DEFAULT '',
  FOREIGN KEY (word_id) REFERENCES words (id),
  FOREIGN KEY (meaning_id) REFERENCES meanings (id)
);

CREATE TABLE word_declensions (
  word_id INTEGER PRIMARY KEY NOT NULL,
  gender TEXT,
  number TEXT NOT NULL,
  FOREIGN KEY (word_id) REFERENCES words (id),
  FOREIGN KEY (gender) REFERENCES genders (id),
  FOREIGN KEY (number) REFERENCES numbers (id)
);

CREATE TABLE word_articles (
  word_id INTEGER PRIMARY KEY NOT NULL,
  definite TEXT NOT NULL,
  indefinite TEXT NOT NULL,
  FOREIGN KEY (word_id) REFERENCES words (id)
);

DB_FILE := FurEnIt.db

.PHONY: git-smudge
git-smudge: git-config
	rm -f $(DB_FILE)
	git restore $(DB_FILE)

.PHONY: git-config
git-config: .gitconfig
	git config --local include.path ../$<

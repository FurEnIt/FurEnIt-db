#! /usr/bin/env bash

FILE="$1"
OUT=$(mktemp)

(cat "${FILE%.*}.schema.sql" && cat) | sqlite3 "$OUT"
cat "$OUT"
rm -f "$OUT"

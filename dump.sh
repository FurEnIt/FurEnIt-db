#! /usr/bin/env bash

DB=$(mktemp)

cat >"$DB"
sqlite3 "$DB" .dump | grep -Pzo "(?s)(?<=\n|^)INSERT.*?\);[ \t]*(\n|$)" | tr -d '\0'
rm -f "$DB"
